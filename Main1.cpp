#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <chrono>
#include <ctime>

#include <nlohmann/json.hpp>
#include <crossguid/guid.hpp>

using json = nlohmann::json;

int main(){
    std::random_device _random_device;
    std::mt19937 _generator(_random_device());
    std::uniform_int_distribution<> _temp_dis(15, 25);
    std::uniform_int_distribution<> _mois_dis(20, 45);

    auto _dt = std::chrono::system_clock::now();
    std::time_t _datetime = std::chrono::system_clock::to_time_t(_dt);

    json _sensors_data;

    for (int i = 0; i < 20; ++i){
        std::string _sensor_name;
        auto _guid = xg::newGuid();
        _sensor_name = (std::string)"Sensor " + (std::string)_guid;

        _sensors_data[_sensor_name] = {
            {"datetime", std::ctime(&_datetime)},
            {"temperature", _temp_dis(_generator)},
            {"moisture", _mois_dis(_generator)}
        };
    }

    std::string _json_data = _sensors_data.dump();

    std::ofstream _json_file("data/data0.json");
    _json_file << _json_data;
    _json_file.close();

    return 0;
}