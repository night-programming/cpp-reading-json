#include <fstream>
#include <iostream>
#include <string>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

int main(){
    std::ifstream json_file("data/example0.json");
    std::string json_file_content(
        (std::istreambuf_iterator<char>(json_file)),
        (std::istreambuf_iterator<char>()) 
    );

    auto json_data = json::parse(json_file_content);

    std::cout << "Host: " << json_data["configuration"]["host"] << "\n";
    std::cout << "Port: " << json_data["configuration"]["port"] << "\n";

    return 0;
}